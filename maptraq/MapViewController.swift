//
//  MapViewController.swift
//  maptraq
//
//  Copyright © 2018 dannyharris. All rights reserved.
//  Unauthorized copy or display, via any medium, is strictly forbidden.
//  Propietary and Confidential.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView?
    var lastDrawn: CLLocationCoordinate2D?
    var overlays = [MKOverlay]()

    /// MKMapView centerer
    /// - Parameters:
    ///   - location: A CLLocationCoordinate2D pointing to the center to adjust to.
    ///   - deltas: A MKCoordinateSpan to render on map. This property defaults to 0.01 deltas.
    func centerMapOn(location: CLLocationCoordinate2D, deltas: MKCoordinateSpan? = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)) {
        if let map = mapView, let span = deltas {
            let region = MKCoordinateRegion(center: location, span: span)
            map.setRegion(region, animated: true)
        } else {
            print("Map seems to be unavailable.")
        }
    }

    /// This method draws a polyline between the given location and
    /// the last given location on the current mapView if possible.
    ///
    /// - Parameter location: A CLLocationCoordinate2D Location
    ///     representing a new location.
    func drawPolyLine(location: CLLocationCoordinate2D) {
        guard let from = lastDrawn else {
            lastDrawn = location
            return
        }
        let polyline = MKPolyline(coordinates: [from, location], count: 2)
        overlays.append(polyline)
        mapView?.add(polyline)
        lastDrawn = location
    }
    
    // MARK: - MKMapViewDelegate

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolyline {
            let renderer = MKPolylineRenderer(overlay: overlay)
            renderer.strokeColor = .blue
            renderer.lineWidth = 5
            return renderer
        }
        return MKOverlayRenderer()
    }
}

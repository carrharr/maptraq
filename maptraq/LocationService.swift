//
//  LocationService.swift
//  maptraq
//
//  Copyright © 2018 dannyharris. All rights reserved.
//  Unauthorized copy or display, via any medium, is strictly forbidden.
//  Propietary and Confidential.
//

import MapKit
import CoreData

protocol LocationServiceDelegate {
    func locationDidUpdate(location: CLLocationCoordinate2D)
}

/// This service takes care of the CLLocationManager and the
/// transmission and storage of its data as trips or for
/// display
class LocationService: NSObject, CLLocationManagerDelegate {

    let queue = DispatchQueue(label: "eu.dannyharris.maptraq.locationservice")
    var currentLocation: CLLocationCoordinate2D?
    let locationManager = CLLocationManager()
    var delegate: LocationServiceDelegate
    var isActive: Bool = false
    var activationPossible: Bool {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined, .restricted, .denied:
            return false
        default:
            return true
        }
    }
    var currentStartDate: Date?
    var currentJourneyPath: [CLLocation]?

    /// Initialiser for LocationService.
    ///
    /// - Parameter delegate: A delegate for this object. This is required to
    ///     perform map updates.
    init(delegate: LocationServiceDelegate) {
        self.delegate = delegate
        super.init()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.delegate = self
        queue.async {
            self.locationManager.startUpdatingLocation()
        }
    }

    /// Start recording method, this will record startDate and start a path
    /// locally.
    func  startRecording() {
        isActive = true
        currentStartDate = Date()
        currentJourneyPath = [CLLocation]()
    }

    /// Stop recording and try to save current trip to context.
    func stopRecording() {
        isActive = false
        let endDate = Date()
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let context = appDelegate!.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Journeys", in: context)!
        let journey = NSManagedObject(entity: entity, insertInto: context)
        if let existingJourney = currentJourneyPath,
            let startDate = currentStartDate, existingJourney.count > 0 {
            let path = NSKeyedArchiver.archivedData(withRootObject: existingJourney)
            journey.setValue(startDate, forKey: "startDate")
            journey.setValue(endDate, forKey: "endDate")
            journey.setValue(path, forKey: "locations")
            do {
                try context.save()
            } catch {
                print("Error occured saving data: \(error)")
            }
        } else {
            // Tell delegate that saving failed
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last, location.horizontalAccuracy > 0 {
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            delegate.locationDidUpdate(location: center)
            if isActive {
                // Only store location when active.
                currentJourneyPath?.append(location)
            }
        }
    }
}

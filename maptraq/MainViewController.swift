//
//  MainViewController.swift
//  maptraq
//
//  Copyright © 2018 dannyharris. All rights reserved.
//  Unauthorized copy or display, via any medium, is strictly forbidden.
//  Propietary and Confidential.
//

import UIKit
import MapKit


/// This class contains the main screen controller and functionality.
///
/// Apart from controlling the screen buttons and navigation, it contains:
/// - LocationService (Optional)
///
/// And conforms to:
/// - LocationServiceDelegate
class MainViewController: MapViewController, LocationServiceDelegate {
    
    @IBOutlet weak var mainButton: UIButton?
    @IBAction func mainButtonWasTapped(_ sender: Any) { self.mainPress() }
    @IBAction func followLocation(_ sender: Any) { mapView?.userTrackingMode = .followWithHeading }
    private var locationService: LocationService?
    private var isTrackingActive: Bool = false
    private var isStopwatchVisible: Bool = false
    /// Controls the time the end trip option is visible
    private var stopVisibleTimer: Timer?
    /// Controls the trip time to update the visible timer clock.
    private var tripTimer: Timer?
    /// Number of seconds since started recording.
    private var counter = 0
    private var queue = DispatchQueue(label: "eu.dannyharris.mainviewcontroller.queue")

    override func viewDidLoad() {
        super.viewDidLoad()
        mapView?.delegate = self
        mapView?.showsUserLocation = true
        mapView?.userTrackingMode = .followWithHeading
        NotificationCenter.default.addObserver(self, selector: #selector(self.newLocationServiceIfNeeded), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.releaseLocationServiceIfNeeded), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        newLocationServiceIfNeeded()
    }

    // Overriding both methods below just to avoid the navigation bar here but still
    // have the child controllers inherit from it.
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }

    /// Initialises a LocationService when none is present.
    /// This is called whenever the application enters foreground.
    ///
    ///     - Warning: Using this method from anywhere else than NotificationCenter
    ///     could lead to unexpected behaviour.
    @objc private func newLocationServiceIfNeeded() {
        guard locationService == nil else { return }
        locationService = LocationService(delegate: self)
    }

    /// Releases the LocationService when the application enters
    /// background and tracking is not active.
    ///
    ///     - Warning: Using this method from anywhere else than NotificationCenter
    ///     could lead to unexpected behaviour.
    @objc private func releaseLocationServiceIfNeeded() {
        guard locationService != nil && !isTrackingActive else { return }
        locationService = nil
    }

    /// Use this method to start or stop a Journey, this does not
    /// deallocate the LocationService and will return an alert to
    /// the user if location services have not been granted permission.
    private func startStopRecording() -> Bool {
        guard locationService != nil, locationService?.activationPossible == true else {
            let message = "Location required, please change your permissions in:\n Settings > maptraq > Location"
            let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
        }
        
        queue.sync { [weak self] in
            guard let strongSelf = self else {
                // Not handling, this should never happen, but better than crashing
                return
            }
            if isTrackingActive {
                strongSelf.isTrackingActive = false
                strongSelf.locationService?.stopRecording()
                strongSelf.lastDrawn = nil
                DispatchQueue.main.async {
                    strongSelf.mapView?.removeOverlays(strongSelf.overlays)
                    strongSelf.overlays.removeAll()
                }
            } else {
                strongSelf.isTrackingActive = true
                strongSelf.locationService?.startRecording()
            }
        }
        return true
    }
    
    private func mainPress() {
        if !isTrackingActive {
            if startStopRecording() {
                transitionButton(button: mainButton!, label: "00:00:00", colour: #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1))
                isStopwatchVisible = true
                tripTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (_) in
                    let label = self.computeTimerString(addingSeconds: 1)
                    if self.isStopwatchVisible && self.isTrackingActive {
                        self.mainButton?.setTitle(label, for:.normal)
                        self.isStopwatchVisible = true
                    }
                }
            }
        } else if isTrackingActive && !isStopwatchVisible {
            startStopRecording() // Not checking, It should return to start no matter what
            stopVisibleTimer?.invalidate()
            stopVisibleTimer = nil
            tripTimer?.invalidate()
            tripTimer = nil
            counter = 0
            transitionButton(button: mainButton!, label: "Start Journey", colour: #colorLiteral(red: 0.2374400198, green: 0.6492436528, blue: 0, alpha: 1))
        } else {
            self.isStopwatchVisible = false
            transitionButton(button: mainButton!, label: "End Journey", colour: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1))
            stopVisibleTimer = Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false) { (_) in
                if self.isTrackingActive {
                    self.transitionButton(button: self.mainButton!, label: self.computeTimerString(), colour: .blue, transition: .transitionFlipFromRight)
                    self.isStopwatchVisible = true
                }
            }
        }
    }
    
    
    /// Transition a button flipFromLeft, or given transition.
    ///
    /// - Parameters:
    ///   - button: UIButton to transition
    ///   - label: Label to set as text for the button.
    ///   - colour: Colour to give the button.
    ///   - transition: Transition to use for the button (Defaults to .transitionFlipFromLeft).
    private func transitionButton(button: UIButton, label: String, colour: UIColor, transition: UIViewAnimationOptions = .transitionFlipFromLeft) {
        UIView.transition(with: button, duration: 0.2,
                          options: transition,
                                  animations: {
                                    button.backgroundColor = colour
                                    button.setTitle(label, for:.normal)
        }, completion: nil)
    }
    
    
    /// Get a string indicating the current counter time.
    ///
    /// - Parameter addingSeconds: Seconds to add to counter when calling this method. This defaults to 0
    /// - Returns: The string indicating the current counter time in a HH:mm:ss format
    private func computeTimerString(addingSeconds: Int = 0) -> String {
        counter += addingSeconds
        let hours = counter / 3600
        let min = counter / 60 % 60
        let sec = Int(counter) % 60
        return String(format:"%02i:%02i:%02i", hours, min, sec)
    }

    // MARK: - LocationServiceDelegate

    func locationDidUpdate(location: CLLocationCoordinate2D) {
        if isTrackingActive { drawPolyLine(location: location) }
    }
}

//
//  JourneyViewController.swift
//  maptraq
//
//  Copyright © 2018 dannyharris. All rights reserved.
//  Unauthorized copy or display, via any medium, is strictly forbidden.
//  Propietary and Confidential.
//

import UIKit
import MapKit

class JourneyViewController: MapViewController {

    @IBOutlet weak var dateLabel: UILabel?
    @IBOutlet weak var totalTimeLabel: UILabel?
    @IBOutlet weak var startLabel: UILabel?
    @IBOutlet weak var endLabel: UILabel?
    @IBOutlet weak var bottomView: UIView?
    let queue = DispatchQueue(label: "eu.dannyharris.maptraq.journeyviewcontroller")
    var journey: Journeys?

    override func viewDidLoad() {
        super.viewDidLoad()
        startLabel?.text = (journey?.startDate)!.stringFromDate(format: "HH:mm")
        endLabel?.text = (journey?.endDate)!.stringFromDate(format: "HH:mm")
        totalTimeLabel?.text = ((journey?.endDate)!.timeIntervalSince1970 -
            (journey?.startDate)!.timeIntervalSince1970).stringFromTimeInterval()
        dateLabel?.text = (journey?.startDate)!.stringFromDate(format: "MMM dd, yyyy")
        mapView?.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Modifications to map should be done here, not before.
        loadJourney { (polyline) in
            DispatchQueue.main.async {
                guard let journey = polyline, let bottom = self.bottomView?.frame.height else {
                    self.displayErrorAndDismiss()
                    return
                }
                self.mapView?.add(journey)
                self.mapView?.setVisibleMapRect(journey.boundingMapRect,
                                                edgePadding: UIEdgeInsetsMake(20, 20, bottom, 20),
                                                animated: false)
            }
        }
    }

    /// Loads the current journey in memory
    ///
    /// - Parameter completion: Callback for operation, returns:
    /// - Parameter polyline: (MKPolyline?) A journey path.
    private func loadJourney(completion: @escaping (_ polyline: MKPolyline?) -> Void) {
        queue.async { [weak self] in
            if let data = self?.journey?.locations,
                let locations = NSKeyedUnarchiver.unarchiveObject(with: data) as? [CLLocation],
                locations.count > 0 {
                var coordinates = [CLLocationCoordinate2D]()
                for location in locations {
                    let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                    coordinates.append(center)
                }
                let polyline = MKPolyline(coordinates: coordinates, count: locations.count)
                completion(polyline)
            } else {
                completion(nil)
            }
        }
    }
    
    private func displayErrorAndDismiss() {
        let message = "Could not display journey"
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            self.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

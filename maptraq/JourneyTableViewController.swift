//
//  JourneyTableViewController.swift
//  maptraq
//
//  Copyright © 2018 dannyharris. All rights reserved.
//  Unauthorized copy or display, via any medium, is strictly forbidden.
//  Propietary and Confidential.
//

import UIKit
import CoreData

class JourneyTableViewController: UITableViewController {

    var journeys: [Journeys]?
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    private var queue = DispatchQueue(label: "eu.dannyharris.journeytableviewcontroller.queue")

    /// This label acts as the background view in the case that no journeys are stored
    /// or retrievable.
    lazy var noJourneysBackgroundView: UILabel = {
        let label = UILabel()
        label.text = "There are no journeys available.\n"
        label.numberOfLines = 0
        label.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        label.textAlignment = .center
        return label
    }()

    /// Gets current stored journeys if possible
    ///
    /// - Parameter completion: Callback for operation success or failure.
    /// - Parameter success: (Bool) Represents wether or not journeys have been succesfully retrieved.
    private func getCurrentJourneys(completion: @escaping (_ success: Bool) -> Void) {
        // Running method in separate queue and with a completion handler
        // to ensure user experience is not affected.
        // TODO: - Retrieving the data to the tableView in viewing batches would improve user experience.
        queue.async { [weak self] in
            let context = self?.appDelegate?.persistentContainer.viewContext
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Journeys")
            do {
                let result = try context?.fetch(request)
                if let journeys = result as? [Journeys] {
                    self?.journeys = journeys.filter { $0.startDate != nil && $0.endDate != nil }
                    if self?.journeys?.count != 0 { completion(true) }
                }
            } catch { }
            completion(false)
        }
    }

    // MARK: - UITableViewDataSource

    override func numberOfSections(in tableView: UITableView) -> Int {
        // Added conditions to ensure user has some meaningful output
        if journeys == nil {
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            tableView.backgroundView = activityIndicator
            activityIndicator.startAnimating()
            getCurrentJourneys { (_) in
                DispatchQueue.main.async {
                    tableView.reloadData()
                }
            }
        } else if journeys?.count == 0 {
            tableView.backgroundView = noJourneysBackgroundView
        } else {
            tableView.backgroundView = nil
        }
        return journeys != nil ? 1 : 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return journeys?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "journeyCell") as! JourneyCell
        let journey = journeys?[indexPath.item]
        cell.date?.text = (journey?.startDate)!.stringFromDate(format: "MMM dd, yyyy")
        let total = (journey?.endDate)!.timeIntervalSince1970 - (journey?.startDate)!.timeIntervalSince1970
        cell.totalTime?.text = "\(total.stringFromTimeInterval()) Total"
        return cell
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let context = appDelegate?.persistentContainer.viewContext
            if let object = journeys?[indexPath.item] {
                context?.delete(object)
                journeys?.remove(at: indexPath.item)
                do { try context?.save() } catch { print("error deleting") } // TODO: - Properly handle catch
                tableView.reloadData()
            }
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let journey = journeys?[indexPath.item] else { return }
        performSegue(withIdentifier: "loadJourney", sender: journey)
    }
}

// MARK: - Segues

extension JourneyTableViewController {

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "loadJourney", let controller = segue.destination as? JourneyViewController {
            let journey = sender as? Journeys
            controller.journey = journey
        }
    }

}

//
//  Utils.swift
//  maptraq
//
//  Copyright © 2018 dannyharris. All rights reserved.
//  Unauthorized copy or display, via any medium, is strictly forbidden.
//  Propietary and Confidential.
//

import Foundation

extension TimeInterval {
    func stringFromTimeInterval() -> String {
        let ti = NSInteger(self)
        let seconds = ti % 60
        let minutes = (ti / 60) % 60
        let hours = (ti / 3600)
        return String(format: "%0.2d:%0.2d:%0.2d",hours,minutes,seconds)
    }
}

extension Date {
    func stringFromDate(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        let stringTime = formatter.string(from: self)
        return stringTime
    }
}

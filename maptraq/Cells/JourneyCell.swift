//
//  JourneyCell.swift
//  maptraq
//
//  Copyright © 2018 dannyharris. All rights reserved.
//  Unauthorized copy or display, via any medium, is strictly forbidden.
//  Propietary and Confidential.
//

import UIKit

class JourneyCell: UITableViewCell {
    
    @IBOutlet weak var date: UILabel?
    @IBOutlet weak var totalTime: UILabel?

}
